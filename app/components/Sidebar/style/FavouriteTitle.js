import styled from 'styled-components';

export const FavouriteTitle = styled.h2`
  margin-bottom: 5px;
`;
