import styled from 'styled-components';

export const Header = styled.div`
  height: 35px;
  width: 100%;
  background: blue;
`;

export const HeaderTitle = styled.a`
  font-size: 16px;
  color: #fff;
  float: left;
  display: inline-block;
  padding-top: 5px;
  padding-left: 15px;
`;
