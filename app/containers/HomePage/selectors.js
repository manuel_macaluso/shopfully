/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectHomePage = state => state.get('home');

const makeSelectFlyer = () =>
  createSelector(selectHomePage, globalState =>
    globalState.get('flyer').toJS(),
  );

export { makeSelectFlyer };
