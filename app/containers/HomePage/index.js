/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import Grid from '@material-ui/core/Grid';
import { makeSelectFlyer } from './selectors';
import { loadFlyer } from './actions';
import reducer from './reducer';
import saga from './saga';
import Card from './components/Card';
import { GridPadding } from './style/GridFlyer';

export class HomePage extends React.PureComponent {
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    this.props.loadFlyer();
  }

  render() {
    const { flyer } = this.props;
    const flyerToRender =
      flyer.length > 0 &&
      flyer.map((singleFlyer, i) => (
        <Grid item xs={6} sm={3} key={i}>
          <Card item={singleFlyer} />
        </Grid>
      ));

    return (
      <article>
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="A ShoFully application homepage"
          />
        </Helmet>
        <div>
          <GridPadding>
            <Grid container spacing={24} className="grid_flyer">
              {flyerToRender}
            </Grid>
          </GridPadding>
        </div>
      </article>
    );
  }
}

HomePage.propTypes = {
  flyer: PropTypes.array,
};

export function mapDispatchToProps(dispatch) {
  return {
    loadFlyer: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadFlyer());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  flyer: makeSelectFlyer(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
