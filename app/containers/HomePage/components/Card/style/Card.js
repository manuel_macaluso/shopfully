import styled from 'styled-components';

export const CardStyle = styled.div`
  .text_ellipsis {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    min-height: 1rem;
  }

  .retailer_name {
    font-size: 11px;
  }

  .flyer_title {
    font-weight: bold;
    font-size: 20px;
    text-transform: capitalize;
  }

  .category_name {
    font-size: 11px;
    opacity: 0.7;
    padding-top: 5px;
    text-transform: capitalize;
  }

  .single-card {
    padding-bottom: 0px;
  }
`;
