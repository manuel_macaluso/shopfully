/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';
import { writeFlyer } from './actions';

import { LOAD_FLYER } from './constants';

/**
 * Github repos request/response handler
 */
export function* getFlyer() {
  // Select username from store
  const requestURL = `https://jsonplaceholder.typicode.com/photos?albumId=1`;

  try {
    // Call our request helper (see 'utils/request')
    const flyer = yield call(request, requestURL);
    yield put(writeFlyer(flyer));
  } catch (err) {
    console.log('Error: ', err);
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_FLYER, getFlyer);
}
