import styled from 'styled-components';

export const GridPadding = styled.div`
  .grid_flyer {
    padding: 1rem;
  }
`;
