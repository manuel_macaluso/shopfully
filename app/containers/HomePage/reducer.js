import { fromJS } from 'immutable';

import { WRITE_FLYER } from './constants';

// The initial state of the App
export const initialState = fromJS({
  flyer: [],
});

function postReducer(state = initialState, action) {
  switch (action.type) {
    case WRITE_FLYER:
      return state.set('flyer', fromJS(action.flyer));
    default:
      return state;
  }
}

export default postReducer;
