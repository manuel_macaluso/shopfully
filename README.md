# Installation
### Clone

- git clone https://manuel_macaluso@bitbucket.org/manuel_macaluso/shopfully.git
- cd shopfully

### Install

- npm install

### Start 

- npm start
